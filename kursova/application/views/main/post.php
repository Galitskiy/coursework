<!--<header class="masthead" style="background-image: url('/public/materials/<?php echo $data['id']; ?>.jpg')">-->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="page-heading">
                <h2><?php echo htmlspecialchars($data['name'], ENT_QUOTES); ?></h2>
            </div>
        </div>
        <div class="col-lg-8 col-md-10 mx-auto">
            <img src="/public/materials/<?php echo $data['id']; ?>.jpg" class="rounded mx-auto d-block shadow-lg" alt="фото" height="350px">
        </div>
        <div class="col-lg-8 col-md-10 mx-auto">
            <p><?php echo htmlspecialchars($data['text'], ENT_QUOTES); ?></p>
        </div>
    </div>
</div>