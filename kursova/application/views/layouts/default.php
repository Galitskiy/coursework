<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Ластівка</title>

        <link href="/public/styles/bootstrap.css" rel="stylesheet">
        <link href="/public/styles/admin.css" rel="stylesheet">
        <link href="/public/styles/font-awesome.css" rel="stylesheet">
        <script src="/public/scripts/jquery.js"></script>
        <script src="/public/scripts/form.js"></script>
        <script src="/public/scripts/popper.js"></script>
        <script src="/public/scripts/bootstrap.js"></script>
        <link href="/public/styles/main.css" rel="stylesheet">



    </head>
    <body class="bg-light">
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	<!--
	
	
	
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
                <img src="../public/images/l.png" alt="logo.png" style="width: 7%;margin-left: 15px;margin-top: 10px;">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">ГОЛОВНА</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/about">Про с.Райки</a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link" href="/contact">Написати нам</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/login">Вхід</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
		
		
		
		
		
        <div class="container bg-white border shadow-lg"> 
            <header class="masthead"  style="background-image: url('/public/images/photo-1565298377077-b3318dd869cb.jpg')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 col-md-12 mx-auto">
                            <div class="site-heading">
                                <h2 class="display-4" style="text-shadow: 2px 2px #000000;">Райківський Будинок Культури</h2>
                                <span class="subheading"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </header>	


		-->

			
        <?php echo $content; ?>
		
		<!--
		
		
		
		
        </div>	




        <footer class="container bg-secondary border shadow-lg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-10 mx-auto">
                        <ul class="list-inline text-center">
                            <li class="list-inline-item">
                                <a href="https://www.facebook.com/profile.php?id=100026046515151" target="_blank">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://uk.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B9%D0%BA%D0%B8_(%D0%91%D0%B5%D1%80%D0%B4%D0%B8%D1%87%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B8%D0%B9_%D1%80%D0%B0%D0%B9%D0%BE%D0%BD)" target="_blank">
                                    <span class="fa-stack fa-lg">
                                        <i class="fa fa-circle fa-stack-2x"></i>
                                        <i class="fa fa-wikipedia-w fa-stack-1x fa-inverse"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <p class="copyright text-white">&copy; 2020, Райківський будинок культури</p>
                    </div>
                </div>
            </div>
        </footer>
		
		
		-->
		

		
		
		
		
    </body>
</html>